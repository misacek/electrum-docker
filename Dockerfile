# version: 0.0.2
# description: Electrum BitCoin wallet in Docker container, this image uses X11 socket
#
# Howto:
#
#  build image
#   git clone https://github.com/loadaverage/electrum.git
#   cd electrum && docker build -t electrum .
#
#  run container
#   docker run --rm \
#        -v ~/.electrum:/home/electrum \
#        -v ~/.themes:/home/electrum/.themes:ro \
#        -v ~/.fonts:/home/electrum/.fonts:ro \
#        -v ~/.icons:/home/electrum/.icons:ro \
#        -v /usr/share/themes:/usr/share/themes:ro \
#        -v /usr/share/fonts:/usr/share/fonts:ro \
#        -v /tmp/.X11-unix:/tmp/.X11-unix \
#        -e DISPLAY=$DISPLAY electrum

FROM debian:stretch

ARG BUILD_DATE="unknown"
ARG BUILD_VERSION=1
ARG CONTAINER_USER=electrum
ARG CONTAINER_USER_GID=977
ARG CONTAINER_USER_ID=5005
ARG DEBIAN_FRONTEND=noninteractive
ARG PACKAGES="\
electrum \
python-pbkdf2 \
python-qt4 \
"

LABEL maintainer="Michal Novacek <michal.novacek@gmail.com>"
LABEL description="\
Dockerfile used to build this image is here: \
https://gitlab.com/misacek/ethereum-docker/blob/master/Dockerfile \
"
LABEL build_version="$BUILD_VERSION"
LABEL build_date="$BUILD_DATE"

ENV QT_X11_NO_MITSHM 1

RUN \
  useradd \
    --uid $CONTAINER_USER_ID \
    --create-home \
    --shell /bin/bash \
    $CONTAINER_USER && \
  apt-get update -qq && \
  apt-get install -y --no-install-recommends $PACKAGES

USER $CONTAINER_USER
CMD ["electrum"]
